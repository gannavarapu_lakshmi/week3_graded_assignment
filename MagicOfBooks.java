package com.greatLearning.assignment3;

import java.util.*;
public class MagicOfBooks<T>{

	Scanner scanner=new Scanner(System.in);


	HashMap<Integer,Book> bmap=new HashMap<>();
	TreeMap<Double,Book> tmap=new TreeMap<>();

	ArrayList<Book> blist=new ArrayList<>();
	//adding books
	public  void addbook(){

		Book b= new Book();
		System.out.println("Enter book id : ");
		b.setId(scanner.nextInt());

		System.out.println("Enter book name : ");
		b.setName(scanner.next());

		System.out.println("Enter book price : ");
		b.setPrice(scanner.nextDouble());

		System.out.println("Enter book genre:");
		b.setGenre(scanner.next());

		System.out.println("Enter number of copies sold : ");
		b.setNoOfCopiesSold(scanner.nextInt());

		System.out.println("Enter book status: ");
		b.setBookstatus(scanner.next());

		bmap.put(b.getId(), b);
		System.out.println("Book added successfully");

		tmap.put(b.getPrice(), b);
		blist.add(b);

	}
	//deleting books
	public void deletebook() throws CustomException{
		if(bmap.isEmpty()) {
			throw new CustomException("no books are available");
		}
		else {
			System.out.println("Enter book id you want to delete : ");
			int id=scanner.nextInt();
			bmap.remove(id);	
			System.out.println("successfully deleted!!");
		}


	}
	//updating books
	public void updatebook() throws CustomException {
		if(bmap.isEmpty()) {
			throw new CustomException("No books are available to update!!");
		}else {


			Book b= new Book();
			System.out.println("Enter book id : ");
			b.setId(scanner.nextInt());

			System.out.println("Enter book name : ");
			b.setName(scanner.next());

			System.out.println("Enter book price : ");
			b.setPrice(scanner.nextDouble());

			System.out.println("Enter book genre : ");
			b.setGenre(scanner.next());

			System.out.println("Enter number of copies sold : ");
			b.setNoOfCopiesSold(scanner.nextInt());

			System.out.println("Enter book status : ");
			b.setBookstatus(scanner.next());

			bmap.replace(b.getId(), b);
			System.out.println("Book details Updated successfully!!");
		}
	}
	//displaying books
	public void displayBookInfo() throws CustomException {

		if (bmap.size() > 0) 
		{
			Set<Integer> keySet = bmap.keySet();

			for (Integer key : keySet) {

				System.out.println(key + " ----> " + bmap.get(key));
			}
		} else {
			throw new CustomException("BookMap is Empty");
		}

	}
	//counting
	public void count() throws CustomException {
		if(bmap.isEmpty()) {
			throw new CustomException("book store is empty");
		}else

			System.out.println("Number of books in the store : "+bmap.size());

	}	
	//autobiography search
	public void autobiography() throws CustomException {
		String bestSelling = "Autobiography";
		if(blist.isEmpty()) {
			throw new CustomException("Book store is Empty");
		}
		else {

			ArrayList<Book> genreBookList = new ArrayList<Book>();

			Iterator<Book> iter=(blist.iterator());

			while(iter.hasNext())
			{
				Book b1=(Book)iter.next();
				if(b1.genre.equals(bestSelling)) 
				{
					genreBookList.add(b1);
					System.out.println(genreBookList);
				}
			} 
		}
	}
	//displaying by feature
	public void displayByFeature(int flag) throws CustomException{

		if(flag==1)//low to high
		{
			if (bmap.size() > 0) 
			{
				Set<Double> keySet = tmap.keySet();
				for (Double key : keySet) {
					System.out.println(key + " ----> " + tmap.get(key));
				}
			} else {
				throw new CustomException("book store is empty is Empty");
			}
		}
		if(flag==2) //high to low		
		{
			Map<Double, Object> ReverseOrder= new TreeMap<>(tmap);
			// putting values in navigable map
			NavigableMap<Double, Object> nmap = ((TreeMap<Double, Object>) ReverseOrder).descendingMap();

			System.out.println("Book Details : ");

			if (nmap.size() > 0) 
			{
				Set<Double> keySet = nmap.keySet();
				for (Double key : keySet) {
					System.out.println(key + " ----> " + nmap.get(key));
				}
			}else{
				throw new CustomException("book store is Empty");
			}

		}

		if(flag==3) //best selling
		{
			String bestSelling = "B";
			ArrayList<Book> genreBookList = new ArrayList<Book>();

			Iterator<Book> iter=blist.iterator();

			while(iter.hasNext())
			{
				Book b=(Book)iter.next();
				if(b.bookstatus.equals(bestSelling)) 
				{
					genreBookList.add(b);

				}

			}
			if(genreBookList.isEmpty())
			{
				throw new CustomException("no best selling books are available");
			}
			else
				System.out.println("best selling books : "+genreBookList);

		}
	}	
}
