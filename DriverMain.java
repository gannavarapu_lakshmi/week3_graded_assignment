package com.greatLearning.assignment3;

import java.util.Scanner;
public class DriverMain {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		MagicOfBooks<Book> mb =new MagicOfBooks<Book>();
		
		while (true) {
			System.out.println("choose option from below:\n"
					+ "1.adding books\n"
					+ "2.delete book\n" + "3.update a book\n"
					+ "4.display all books\n" + "5.Total count of books\n"
					+ "6.Search autobiography books" +  "\n7.To display by features");

			System.out.println("Enter your choice : ");
			int choice = sc.nextInt();

			switch (choice) {

			case 1://Adding a book
				System.out.println("Enter no of books you want to add : ");
				int n=sc.nextInt();

				for(int i=1;i<=n;i++) {
					mb.addbook();
				}
				break;

			case 2://Deleting a book
				try {
					mb.deletebook();
				}catch(CustomException e) {
					System.out.println(e.getMessage());
				}				
				break;
			case 3://Update a book
				try {
					mb.updatebook();
					
				}
				catch(CustomException e)
				{
					System.out.println(e.getMessage());
				}
				break;
			case 4://displaying books
				try 
				{
					mb.displayBookInfo();
				}
				catch(CustomException e)
				{
					
					System.out.println(e.getMessage());
				}
				break;

			case 5://counting books
				try {
					mb.count();
					
				}catch(CustomException e){
					System.out.println(e.getMessage());
				}
		
				break;

			case 6://autobiography search
				try {
					mb.autobiography();
					
				}catch(CustomException e) {
					System.out.println(e.getMessage());
					
				}
				
				break;


			case 7://search by feature
				System.out.println("Enter your choice : \n 1. Price low to high "
						+ "\n 2.Price high to low \n 3. Best selling");
				int choice1 = sc.nextInt();

				switch (choice1) {

				case 1 : try {
					mb.displayByFeature(1);
				} catch (CustomException e) {
					// TODO Auto-generated catch block
					System.out.println(e.getMessage());
				}
				break;
				case 2:try {
					mb.displayByFeature(2);
				} catch (CustomException e) {
					// TODO Auto-generated catch block
					System.out.println(e.getMessage());
				} 
				break;
				case 3:try {
					mb.displayByFeature(3);
				} catch (CustomException e) {
					// TODO Auto-generated catch block
					System.out.println(e.getMessage());
				}
				break;
				}

			default:
				System.out.println("You've entered wrong choice.!");

			}

		}

	}
}

